require essioc
require mrfioc2, 2.3.1+14

# Config
epicsEnvSet "PEVM" "TD-D24:Ctrl-EVM-1:"
epicsEnvSet "PCIID" "0e:00.0"

# EVM
iocshLoad "$(mrfioc2_DIR)/evmEss.iocsh" "P=$(PEVM),TD="

# Load standard module startup scripts
iocshLoad $(essioc_DIR)/common_config.iocsh

